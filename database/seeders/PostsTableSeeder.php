<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Posts;

class PostsTableSeeder extends Seeder
{
    public function run()
    {

        Posts::create([
            'category_id' => 1,
            'title' => 'Web App Development',
            'description' => 'We developed a responsive web application for a small business using the latest web technologies.',
            'project_date' => '2022-01-01',
            'client_name' => 'Emmanuel Dankwah',
            'designer' => 'Sessah Sadicka',
        ]);

        Posts::create([
            'category_id' => 2,
            'title' => 'Front End Development',
            'description' => 'We created a unique and memorable brand identity for a startup.',
            'project_date' => '2022-02-01',
            'client_name' => 'CARISCA',
            'designer' => 'Sadicka Sessah',
        ]);

        Posts::create([
            'category_id' => 3,
            'title' => 'UI/UX Design',
            'description' => 'We designed a User Interface for a website.',
            'project_date' => '2022-03-01',
            'client_name' => 'Home7',
            'designer' => 'Sadicka Sessah',
        ]);

        Posts::create([
            'category_id' => 3,
            'title' => 'UI/UX Design',
            'description' => 'We designed a User Interface for a website.',
            'project_date' => '2022-03-01',
            'client_name' => 'EduAssistGh',
            'designer' => 'Sadicka Sessah',
        ]);

    }
}
?>
