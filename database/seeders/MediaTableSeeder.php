<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Media;

class MediaTableSeeder extends Seeder
{
    public function run()
    {

        Media::create([
            'posts_id' => 1,
            'media' => 'motion-plus.png',
            'type' => 'image',

        ]);


        Media::create([
            'posts_id' => 2,
            'media' => 'carisca-lms.png',
            'type' => 'image',
        ]);

        Media::create([
            'posts_id' => 3,
            'media' => 'home-7.png',
            'type' => 'image',
        ]);

        Media::create([
            'posts_id' => 4,
            'media' => 'eduassistgh.png',
            'type' => 'image',
        ]);



    }
}
?>
