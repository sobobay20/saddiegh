<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        Category::create([
            'name' => 'Web Development',
            'desc' => 'Web design and development',
        ]);

        Category::create([
            'name' => 'Front End Developmemt',
            'desc' => 'Front end design and development',
        ]);

        Category::create([
            'name' => 'UI/UX Design',
            'desc' => 'UI/UX Design',
        ]);

        Category::create([
            'name' => 'MailChimp',
            'desc' => 'MailChimp Integration',
        ]);

    }
}
?>
