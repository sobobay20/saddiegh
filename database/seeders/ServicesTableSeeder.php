<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Services;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = [
            ['name' => 'Web App Development', 'description' => 'I offer web app development services for startups, enterprises, and more.', 'price' => '5000.00'],
            ['name' => 'Front End Development', 'description' => 'I offer front end development services for websites, landing pages, and more.', 'price' => '2500.00'],
            ['name' => 'UI/UX Design', 'description' => 'I offer UI/UX design services for web and mobile applications, dashboards, and more.', 'price' => '1000.00'],
            ['name' => 'MailChimp Integration', 'description' => 'I offer MailChimp integration services to connect your website or app with your email marketing campaigns.', 'price' => '500.00'],
];

        foreach ($services as $service) {
            Services::create($service);
        }
    }

}


?>
