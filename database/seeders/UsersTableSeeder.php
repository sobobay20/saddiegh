<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $birthday = Carbon::createFromFormat('d F Y', '22 March 1998');
        $age = $birthday->diffInYears(Carbon::now());
        User::create([
            'name' => 'Sadicka Sessah',
            'email' => 'sobobay20@gmail.com',
            'password' => bcrypt('123456'),
            'city' => 'Kumasi, Ghana',
            'about' => 'I am a passionate and experienced developer skilled in using various programming languages and frameworks to create high-quality, efficient, and responsive web applications.',
            'phone' => '+233595494115',
            'website' => 'https://www.motionplusstudios.com/saddiegh/public',
            'degree' => 'Masters',
            'birthday' => $birthday,
            'age' => $age,
            'freelance' => 'Available',
            'role' => 'admin'
        ]);
    }
}



?>
