<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Testimonials extends Model
{
    protected $table = 'testimonials';

    protected $fillable = [
        'name', 'designation', 'message', 'image','post_id'
    ];

    public function post() {
        return $this->belongsTo(Post::class, 'posts_id');
    }

}
