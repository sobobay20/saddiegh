<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Posts;


class Media extends Model
{
    use HasFactory;

    protected $table = 'media';

    protected $fillable = [
        'posts_id', 'media', 'type'
    ];

    public function post()
    {
        return $this->belongsTo( Posts::class, 'posts_id', 'id');
    }
}

