<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Media;
use App\Models\Category;
use App\Models\Testimonials;




class Posts extends Model
{
    protected $table = 'posts';

    protected $fillable = [
        'category_id', 'title', 'description', 'project_date', 'project_url', 'client_name', 'designer'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function testimonials() {
        return $this->hasMany(Testimonials::class, 'posts_id');
    }

    public function media()
    {
        return $this->hasMany(Media::class,  'posts_id', 'id');
    }
}


