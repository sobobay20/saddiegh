<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointments extends Model
{
    protected $fillable = [
        'name', 'email', 'subject', 'message', 'attended_to', 'response_timestamp', 'response_identifier'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($appointment) {
            $appointment->response_identifier = uniqid();
        });
    }
}
