<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\Models\Testimonials;
use App\Models\Category;
use App\Models\Posts;
use Illuminate\Http\Request;
use App\Http\Requests\TestimonialCreateRequest;
use App\Http\Requests\TestimonialUpdateRequest;
class TestimonialsController extends Controller
{
    public function index()
    {
        $testimonials = Testimonials::where('is_deleted', 0)->get();
        return view('admin.testimonials.index', ['testimonials' => $testimonials]);
    }


    public function create()
    {
        $posts = Posts::all();
        return view('admin.testimonials.create', compact('posts'));
    }


    public function store(TestimonialCreateRequest $request)
    {
        $testimonial = new Testimonials();
        $testimonial->name = $request->input('name');
        $testimonial->message = $request->input('message');
        $testimonial->position = $request->input('position');
        $testimonial->posts_id = $request->input('posts_id');


        if($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = $image->getClientOriginalName();
            $path = $image->storeAs('public/assets/uploads/testimonials', $filename);
            // $destinationPath =$image->storeAs('public/assets/uploads/testimonials');
            $testimonial->image = $filename;
        }

        if($testimonial->save()) {
            return route('admin.testimonials.index')->with('success', 'Testimonial created successfully');
        } else {
            return back()->withErrors(['error', 'Unable to create testimonial']);
        }
    }

public function update(TestimonialUpdateRequest $request, $id)
{
    $testimonial = Testimonials::find($id);
    $testimonial->name = $request->input('name');
    $testimonial->message = $request->input('message');
    $testimonial->position = $request->input('position');
    $testimonial->posts_id = $request->input('posts_id');

    if ($request->hasFile('image')) {
        if ($testimonial->image) {
            $oldImagePath = storage_path('/app/public/assets/uploads/') . $testimonial->image;
            if (file_exists($oldImagePath)) {
                unlink($oldImagePath);
            }
        }

        $image = $request->file('image');
        $imageName = $image->getClientOriginalName();
        $destinationPath = storage_path('/app/public/assets/uploads');
        $image->move($destinationPath, $imageName);
        $testimonial->image = $imageName;
    }

    if($testimonial->save()) {
        return back()->with('success', 'Testimonial updated successfully');
    } else {
        return back()->withErrors(['error', 'Unable to update testimonial']);
    }
}



public function edit($id)
{
    $testimonial = Testimonials::find($id);
    $posts = Posts::all();
    return view('admin.testimonials.edit', compact('testimonial', 'posts'));
}


    public function destroy($id)
    {
        $testimonial = Testimonials::findOrFail($id);
        $testimonial->is_deleted = 1;
        $testimonial->save();
        return back()->with('success', 'Testimonial deleted successfully');

    }
}
