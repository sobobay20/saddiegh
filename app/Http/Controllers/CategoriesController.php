<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.index', ['categories' => $categories]);
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'desc' => 'required',
        ]);
        $category = new Category();
        $category->name = $validatedData['name'];
        $category->desc = $validatedData['desc'];

        if($category->save()) {
            return back()->with('success', 'Category created successfully');
        } else {
            return back()->withErrors(['error', 'Unable to create category']);
        }
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.categories.edit', ['category' => $category]);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'desc' => 'required',
        ]);
        $category = Category::findOrFail($id);
        $category->name = $validatedData['name'];
        $category->desc = $validatedData['desc'];
        if($category->save()) {
            return back()->with('success', 'Category updated successfully');
        } else {
            return back()->withErrors(['error', 'Unable to update category']);
        }
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return back()->with('success', 'Category deleted successfully');
    }
}

