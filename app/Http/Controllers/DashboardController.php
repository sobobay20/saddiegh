<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Posts;
use App\Models\Media;
use App\Models\User;
use App\Models\Appointments;
use Illuminate\Support\Facades\Auth;
use Google\Client as Google_Client;
use Google_Service_AnalyticsReporting;
use Google_Service_AnalyticsReporting_ReportRequest;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_OrderBy;
use Illuminate\Support\Facades\Session;


class DashboardController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            $appointments = Appointments::all();
            $recent_posts = Posts::with('media')->latest()->take(5)->get();
            $user = User::find(1);

            return view('admin.dashboard', compact('appointments', 'recent_posts', 'user'));
        } else {
            return redirect('login');
        }
    }




}
