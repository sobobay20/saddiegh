<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CustomLoginController extends Controller
{
    public function index()
    {
        return view('auth.login.login');
    }

    public function store(Request $request)
    {
        // validate the form data
        $validatedData = $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        // attempt to authenticate the user
        if (Auth::attempt($validatedData)) {
            // if successful, redirect to the dashboard
            return redirect()->intended(route('dashboard'));
        }

        // if unsuccessful, redirect back to the login form with an error message
        return redirect()->back()->withErrors(['Invalid email or password']);
    }
}
