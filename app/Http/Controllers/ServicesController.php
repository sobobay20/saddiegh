<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Services;
use App\Models\Testimonials;


class ServicesController extends Controller
{
    // public function __construct()
    // {
    //     $this->middleware(['web','auth', 'authcheck']);
    // }

    public function display()
    {
        $services = Services::all();
        $testimonials = Testimonials::all();
        return view('services', ['services' => $services, 'testimonials' => $testimonials]);
    }

    public function index()
    {
        $services = Services::all();
        return view('admin.services.index', ['services' => $services]);
    }

    public function create()
    {
        return view('admin.services.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'price' => 'required|numeric'
        ]);
        $service = new Services();
        $service->name = $validatedData['name'];
        $service->description = $validatedData['description'];
        $service->price = $validatedData['price'];

        if($service->save()) {
            return back()->with('success', 'Service created successfully');
        } else {
            return back()->withErrors(['error', 'Unable to create service']);
        }

    }

    public function edit($id)
    {
        $service = Services::findOrFail($id);
        return view('admin.services.edit', ['service' => $service]);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required',
            'price' => 'required|numeric'
        ]);
        $service = Services::findOrFail($id);
        $service->name = $validatedData['name'];
        $service->description = $validatedData['description'];
        $service->price = $validatedData['price'];
        if($service->save()) {
            return back()->with('success', 'Service updated successfully');
        } else {
            return back()->withErrors(['error', 'Unable to create service']);
        }
    }

    public function destroy($id)
    {
        $service = Services::findOrFail($id);
        $service->delete();
        return back()->with('success', 'Service deleted successfully');
    }
}

