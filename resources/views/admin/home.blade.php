@extends('layouts.base')

@section('content')
     <!-- ======= Hero Section ======= -->
  <section id="hero" class="hero d-flex flex-column justify-content-center align-items-center" data-aos="fade" data-aos-delay="1500">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-6 text-center">
          <h2>I'm <span>Emmanuel Dankwah, </span> a Professional Videographer from Kumasi, Ghana</h2>
          @include('partials.quote')
        </div>
      </div>
    </div>
  </section><!-- End Hero Section -->

  <main id="main" data-aos="fade" data-aos-delay="1500">

    <!-- ======= Gallery Section ======= -->
    <section id="gallery" class="gallery">
      <div class="container-fluid">
        <div class="row gy-4 justify-content-center">
            @foreach($posts as $post)
                @foreach($post->media as $media)
                    @if($media->media != null)
                        @if($media->type == 'image')
                            <div class="col-4 fh">
                                <div class="gallery-item h-100">
                                    {{-- <a href="{{ route('admin.show', $post->id) }}"> --}}
                                        <img src="{{ Storage::url('assets/uploads/images/'.$media->media) }}" class="img-fluid" alt="{{ $post->title }}">
                                        <div class="gallery-links d-flex align-items-center justify-content-center">
                                            <a href="{{ Storage::url('assets/uploads/images/'.$media->media) }}" title="{{ $post->title }}" class="glightbox preview-link"><i class="bi bi-arrows-angle-expand"></i></a>
                                            <a href="{{ route('posts.show',  $post->id) }}" title="View Details" class="details-link"><i class="bi bi-link-45deg"></i></a>
                                        </div>
                                </div>
                            </div>
                        @elseif($media->type == 'video')
                            <div class="col-4 fh">
                                <div class="gallery-item">
                                    {{-- <a href="{{ route('admin.show', $post->id) }}"> --}}
                                        <video src="{{ Storage::url('assets/uploads/videos/'.$media->media) }}" class="img-fluid" alt="{{ $post->title }}" controls autoplay muted></video>
                                        <div class="gallery-links d-flex align-items-center justify-content-center">
                                            <a href="{{ route('posts.show',  $post->id) }}" title="View Details" class="details-link"><i class="bi bi-link-45deg"></i></a>
                                        </div>
                                    {{-- </a> --}}
                                </div>
                            </div>
                        @endif
                    @endif
                @endforeach
            @endforeach
        </div>


      </div>
    </section><!-- End Gallery Section -->
  </main><!-- End #main -->
@endsection


