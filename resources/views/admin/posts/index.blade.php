<x-main>
    <main id="main" data-aos="fade" data-aos-delay="1500">

        <!-- ======= End Page Header ======= -->
        <div class="page-header d-flex align-items-center">
          <div class="container position-relative">
            <div class="row d-flex justify-content-center">
              <div class="col-lg-6 text-center">
                @if (session('success'))
                <div id="alert" class="alert alert-success">
                  {{ session('success') }}
                </div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                @endif
                <h2>All Projects</h2>
              </div>
            </div>
          </div>
        </div><!-- End Page Header -->

        <section class="sample-page">
          <div class="container" data-aos="fade-up">
            <div class="row mb-2">
                <div class="col-md-2 offset-md-10">
                    <a href="{{ route('admin.posts.create') }}">
                        <button type="button" class="btn btn-sm btn-primary float-right w-150">
                           <i class="fa fa-plus text-white"></i> Add Project
                         </button>
                    </a>
                </div>
            </div>
             <table class="table table-responsive-md  table-responsive-lg table-responsive-sm">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Project Date</th>
                        <th>Client Name</th>
                        <th>Designer</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->title }}</td>
                        <td>{{ date('jS M Y', strtotime( $post->project_date)) }}</td>
                        <td>{{ $post->client_name }}</td>
                        <td>{{ $post->designer }}</td>
                        <td>
                            <a href="{{route('admin.posts.edit', $post->id)}}">
                                <i class="fa fa-edit text-primary"></i>
                            </a>
                            <span>
                                <form action="{{ route('admin.posts.destroy', $post->id) }}" method="POST" class="form-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" onclick="
                                    event.preventDefault();
                                    Swal.fire({
                                        title: 'Are you sure?',
                                        text: 'You will not be able to recover this post!',
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonText: 'Yes, delete it!',
                                        cancelButtonText: 'No, keep it'
                                    }).then((result) => {
                                        if (result.value) {
                                            this.closest('form').submit();
                                        }
                                    });
                                ">
                                    <i class="fa fa-trash text-danger"></i>
                                </button>
                                </form>
                            </span>
                        </td>



                    </tr>
                    @endforeach
                </tbody>
             </table>
          </div>
        </section>


      </main><!-- End #main -->
</x-main>
