<x-main>
    <main id="main" data-aos="fade" data-aos-delay="1500">

        <!-- ======= End Page Header ======= -->
        <div class="page-header d-flex align-items-center">
          <div class="container position-relative">
            <div class="row d-flex justify-content-center">
              <div class="col-lg-6 text-center">
                @if (session('success'))
                <div id="alert" class="alert alert-success">
                  {{ session('success') }}
                </div>
                @endif
                @if (session('status'))
                <div id="alert" class="alert alert-success">
                  {{ session('status') }}
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


                <h2>Testimonial Update</h2>

              </div>
            </div>
          </div>
        </div><!-- End Page Header -->

        <section class="sample-page">
          <div class="container" data-aos="fade-up">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                    <form method="POST" action="{{ route('admin.testimonials.update', ['id' => $testimonial->id])}}" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="text-center">
                                    <div class="preview-container">
                                        <div id="preview-div" class="img-thumbnail" style="background: url({{ $testimonial->image ? Storage::url('public/assets/uploads/testimonials/'.$testimonial->image) : asset('assets/img/avatar.jpg') }}); background-repeat: no-repeat; background-size: cover; height: 200px; width: 100%; border: 3px #fff; border-radius: 5%; ">
                                        </div>
                                        <h6 class="mt-2 mb-2">Change Image</h6>
                                        <input type="file" name="image" class="form-control" onchange="previewImg(this)">
                                      </div>
                                  </div>

                              </div>

                              <div class="col-md-9">
                                <div class="form-group mb-2">
                                    <label class="form-label" for="project-title">Name</label>
                                    <input type="text" class="form-control" name="name" value="{{ $testimonial->name }}"/>
                                </div>

                                <div class="form-group mb-2">
                                    <label class="form-label" for="customFile">Message</label>
                                    <textarea class="form-control" rows="3" name="message">{{ $testimonial->message }}</textarea>
                                </div>
                                <div class="form-group mb-2">
                                    <label class="form-label" for="customFile">Select Post for this Testimonials</label>
                                    <select class="form-control" name="posts_id">
                                       @foreach ($posts as $post)
                                           <option value="{{ $post->id }}" {{ $post->id === $testimonial->post_id ? 'selected' : '' }}>{{ $post->title }}</option>
                                       @endforeach
                                   </select>
                                </div>

                                <div class="form-group mb-2">
                                   <label class="form-label" for="project-title">Position/Job Title</label>
                                   <input type="text" class="form-control" name="position" value="{{ $testimonial->position }}"/>
                               </div>
                              </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-primary mb-2 hover:bg-indigo-700">Submit</button>
                            </div>
                         </div>

                    </form>
                </div>
            </div>

          </div>
        </section>


      </main><!-- End #main -->
</x-main>
