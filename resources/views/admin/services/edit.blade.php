<x-main>
    <main id="main" data-aos="fade" data-aos-delay="1500">

        <!-- ======= End Page Header ======= -->
        <div class="page-header d-flex align-items-center">
          <div class="container position-relative">
            <div class="row d-flex justify-content-center">
              <div class="col-lg-6 text-center">
                @if (session('status'))
                <div id="alert" class="alert alert-success">
                  {{ session('status') }}
                </div>
                @endif

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


                <h2>Service Update</h2>

              </div>
            </div>
          </div>
        </div><!-- End Page Header -->

        <section class="sample-page">
          <div class="container" data-aos="fade-up">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-xl-8 col-md-12 col-sm-12 col-12">
                    <form method="POST" action="{{ route('admin.services.update', ['id' => $service->id])}}" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">


                         <div class="form-group mb-2">
                             <label class="form-label" for="project-title">Name</label>
                             <input type="text" class="form-control" name="name" value="{{ $service->name }}"/>
                         </div>

                         <div class="form-group mb-2">
                             <label class="form-label" for="customFile">Description</label>
                             <textarea class="form-control" rows="3" name="description">{{ $service->description }}</textarea>
                         </div>


                         <div class="form-group mb-2">
                            <label class="form-label" for="project-title">Start Price</label>
                            <input type="text" class="form-control" name="price" value="{{ $service->price }}"/>
                        </div>

                        <button type="submit" class="btn btn-primary mb-2 hover:bg-indigo-700">Submit</button>
                    </form>
                </div>
            </div>

          </div>
        </section>


      </main><!-- End #main -->
</x-main>
