<x-main>
    <main id="main" data-aos="fade" data-aos-delay="1500">

        <!-- ======= End Page Header ======= -->
        <div class="page-header d-flex align-items-center">
          <div class="container position-relative">
            <div class="row d-flex justify-content-center">
              <div class="col-lg-6 text-center">
                @if (session('success'))
                <div id="alert" class="alert alert-success">
                  {{ session('success') }}
                </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                <h2>Categories</h2>
              </div>
            </div>
          </div>
        </div><!-- End Page Header -->

        <section class="sample-page">
          <div class="container" data-aos="fade-up">
            <div class="row mb-2">
                <div class="col-md-3 offset-md-9">
                    <a href="{{ route('admin.categories.create') }}">
                        <button type="button" class="btn btn-sm btn-primary float-right">
                           <i class="fa fa-plus text-white"></i> Add Category
                         </button>
                    </a>
                </div>
            </div>
             <table class="table table-responsive-lg table-responsive-md table-responsive-sm">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                    <tr>
                        <td>
                            {{ $category->name }}
                        </td>
                        <td>{{ $category->desc }}</td>
                        <td>
                            <a href="{{route('admin.categories.edit', $category->id)}}">
                                <i class="fa fa-edit text-primary"></i>
                            </a>
                            <span>
                                <form action="{{ route('admin.categories.destroy', $category->id) }}" method="POST" class="form-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" name="delete" onclick="
                                        event.preventDefault();
                                        Swal.fire({
                                            title: 'Are you sure?',
                                            text: 'You will not be able to recover this category!',
                                            icon: 'warning',
                                            showCancelButton: true,
                                            confirmButtonText: 'Yes, delete it!',
                                            cancelButtonText: 'No, keep it'
                                        }).then((result) => {
                                            if (result.value) {
                                                this.closest('form').submit();
                                            }
                                        });
                                    ">
                                        <i class="fa fa-trash text-danger"></i>
                                    </button>
                                </form>

                            </span>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
             </table>
          </div>
        </section>


      </main><!-- End #main -->
</x-main>
