@extends('layout.port-details')
@section('title', 'Portfolio Details')
@section('content')

<main id="main">
    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="row gy-4">

          <div class="col-lg-8">
            <div class="portfolio-details-slider swiper">
              <div class="swiper-wrapper align-items-center">
                @foreach($post->media as $media)
                @if($media->type === 'video')
                <div class="swiper-slide">
                    <video src="{{ asset('assets/uploads/videos/'.$media->media) }}" type="video/mp4" class="img-fluid" alt="{{ $post->title }}" controls autoplay loop muted>
                    </video>
                  </div>
                @elseif($media->type === 'image')
                  <div class="swiper-slide">
                    <img src="{{ asset('assets/uploads/images/'.$media->media) }}" class="img-fluid" alt="{{ $post->title }}">
                  </div>
                @endif
              @endforeach

              </div>
              <div class="swiper-pagination"></div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="portfolio-info">
              <h3>Project information</h3>
              <ul>
                <li><strong>Category</strong>: {{ $categoryName }}</li>
                <li><strong>Client</strong>: {{ $post->client_name }}</li>
                <li><strong>Project date</strong>:{{  date('jS M Y', strtotime( $post->project_date)) }}</li>
              </ul>
            </div>
            <div class="portfolio-description">
              <h2>{{ $post->title }}</h2>
              <p>
                {{ $post->description }}
              </p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->
@endsection

