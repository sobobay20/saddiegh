@extends('layout.app')
@section('title', 'About')
@section('content')
<main id="main">
    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>About</h2>
          <p>Get to know me better! Here's a little bit about who I am and what I'm passionate about.</p>
        </div>

        <div class="row">
          <div class="col-lg-4">
            @if (!empty($user->profile_picture))
            <img src="{{ asset('assets/uploads/profile/'.$user->profile_picture) }}" class="img-fluid" alt="">
            @else
            <div class="img-thumbn">
            </div>
                {{-- <img src="{{ asset('assets/img/avatar.jpg') }}" class="img-fluid"  alt=""> --}}
            @endif
            {{-- <img src="assets/img/about.jpg" class="img-fluid" alt=""> --}}
          </div>
          <div class="col-lg-8 pt-4 pt-lg-0 content">
            <h3>Developer &amp; UI/UX Designer</h3>
            <p>
                {{ $user->about }}
            </p>
            <div class="row">
              <div class="col-lg-6">
                <ul>
                  {{-- <li><i class="bi bi-rounded-right"></i> <strong>Birthday:</strong> {{ \Carbon\Carbon::parse($user->birthday)->format('d F Y') }}</li> --}}
                  <li><i class="bi bi-rounded-right"></i> <strong>Website:</strong> <a href="{{ $user->website }}" target="blank">www.motionplusstudios.com/saddiegh/public</a></li>
                  <li><i class="bi bi-rounded-right"></i> <strong>Phone:</strong> {{ $user->phone }}</li>
                  <li><i class="bi bi-rounded-right"></i> <strong>City:</strong> {{ $user->city }}</li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul>
                  {{-- <li><i class="bi bi-rounded-right"></i> <strong>Age:</strong> {{ $user->age }}</li> --}}
                  <li><i class="bi bi-rounded-right"></i> <strong>Degree:</strong> {{ $user->degree }}</li>
                  <li><i class="bi bi-rounded-right"></i> <strong>Email:</strong> {{ $user->email }}</li>
                  <li><i class="bi bi-rounded-right"></i> <strong>Freelance:</strong> {{ $user->freelance }}</li>
                </ul>
              </div>
            </div>

          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Skills Section ======= -->
    <section id="skills" class="skills">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Skills</h2>
          <p>Discover my professional skillset! Here are some of the key areas where I excel and can provide value.</p>
        </div>

        <div class="row skills-content">

          <div class="col-lg-6">

            <div class="progress">
              <span class="skill">HTML <i class="val">100%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">CSS <i class="val">90%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">JavaScript <i class="val">75%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
                <span class="skill">Bootstrap <i class="val">95%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            <div class="progress">
                <span class="skill">Adobe XD <i class="val">80%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

          </div>

          <div class="col-lg-6">

            <div class="progress">
              <span class="skill">PHP <i class="val">80%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">WordPress/CMS <i class="val">90%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
              <span class="skill">Photoshop <i class="val">55%</i></span>
              <div class="progress-bar-wrap">
                <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
            </div>

            <div class="progress">
                <span class="skill">Laravel <i class="val">70%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>

            <div class="progress">
                <span class="skill">Figma <i class="val">90%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

          </div>

        </div>

      </div>
    </section><!-- End Skills Section -->


    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Testimonials</h2>
          <p>Don't just take my word for it! See what others have to say about working with me and the impact I've had on their projects.</p>
        </div>

        <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">
            @foreach($testimonials as $testimonial)
            <div class="swiper-slide">
              <div class="testimonial-item">
                @if (!empty($testimonial))
                    <img src="{{ asset('assets/uploads/testimonials/'.$testimonial->image) }}" class="testimonial-img"  style="border-radius: 50%;" alt="">
                    @else
                        <img src="{{ asset('assets/img/avatar.jpg') }}" class="testimonial-img"  style="border-radius: 50%;" alt="">
                    @endif
                {{-- <img src="assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt=""> --}}
                <h3>{{ $testimonial->name }}</h3>
                <h4>{{ $testimonial->position }}</h4>
                <p>
                  <i class="fa fa-quote-left"></i>
                  {{ $testimonial->message }}
                  <i class="fa fa-quote-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->
            @endforeach
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->

  </main><!-- End #main -->
@endsection



