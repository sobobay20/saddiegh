@extends('layout.app')
@section('title', 'Home')
@section('content')
 <!-- ======= Hero Section ======= -->
 <section id="hero" class="d-flex align-items-center">
    <div class="container d-flex flex-column align-items-center" data-aos="zoom-in" data-aos-delay="100">
      <h1>Sadicka Sessah</h1>
      <h2>I'm a Developer from Kumasi, Ghana</h2>
      <a href="{{ route('about')}}" class="btn-about">About Me</a>
    </div>
  </section><!-- End Hero -->
@endsection


