
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container-fluid d-flex justify-content-between align-items-center">

        <a href="/" class="logo d-flex align-items-center  me-auto me-lg-0">
            <!-- Uncomment the line below if you also wish to use an image logo -->
             <img src="{{ asset('assets/uploads/logo/saddie.png') }}" alt="">
            {{-- <i class="bi bi-camera"></i> --}}
            {{-- <h1>MotionPlus Studios</h1> --}}
          </a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="active" href="{{ route('index')}}">Home</a></li>
          <li><a href="{{ route('about')}}">About</a></li>
          {{-- <li><a href="{{ route('resume')}}">Resume</a></li> --}}
          <li><a href="{{ route('services.index')}}">Services</a></li>
          <li><a href="{{ route('posts.index')}}">Portfolio</a></li>
          <li><a href="{{ route('contact')}}">Contact</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

      <div class="header-social-links">
        <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
        <a href="https://web.facebook.com/sadicka.sessah.3/" class="facebook"><i class="bi bi-facebook"></i></a>
        <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
        <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
      </div>

    </div>

  </header><!-- End Header -->
