@extends('layout.app')
@section('title', 'Portfolio')
@section('content')
 <!-- ======= Hero Section ======= -->
    <main id="main">

        <!-- ======= Portfolio Section ======= -->
        <section id="portfolio" class="portfolio">
          <div class="container" data-aos="fade-up">

            <div class="section-title">
              <h2>Portfolio</h2>
              <p>Showcasing my skills and expertise in UI/UX design, frontend development, web app development, and more.</p>
            </div>

            <div class="row" data-aos="fade-up" data-aos-delay="100">
                <div class="col-lg-12 d-flex justify-content-center">
                    <ul id="portfolio-flters">
                        <li data-filter="*" class="filter-active">All</li>
                        @foreach($categories as $category)
                        <li data-filter=".filter-{{ $category->id }}">{{ $category->name }}</li>
                     @endforeach
                    </ul>

                </div>
            </div>


            <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
                @foreach($posts as $post)
                    @foreach($post->media as $media)
                        @if($media->media != null)
                            @if($media->type == 'image' || $media->type == 'video')
                            <div class="col-lg-4 col-md-6 portfolio-item filter-{{ $post->category->id }}">
                                    <div class="portfolio-wrap">
                                        <img src="{{ asset('assets/uploads/'.$media->type.'s/'.$media->media) }}" class="img-fluid" alt="{{ $post->title }}">
                                        <div class="portfolio-info">
                                            <h4>{{ $post->title }}</h4>
                                            <p>{{ $post->description }}</p>
                                            <div class="portfolio-links">
                                                <a href="{{ asset('assets/uploads/'.$media->type.'s/'.$media->media) }}" data-gallery="portfolioGallery" class="portfolio-lightbox" title="{{ $post->title }}"><i class="fa fa-plus"></i></a>
                                                <a href="{{ route('show', $post->id) }}" class="portfolio-details-lightbox" data-glightbox="type: external" title="Portfolio Details"><i class="fa fa-link"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @endif
                        @endforeach
                @endforeach
            </div>


          </div>
        </section><!-- End Portfolio Section -->

      </main><!-- End #main -->

@endsection
