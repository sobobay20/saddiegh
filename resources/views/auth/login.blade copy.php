<x-login>
    <main id="main" data-aos="fade" data-aos-delay="1500">

        <!-- ======= End Page Header ======= -->
        <div class="page-header d-flex align-items-center">
          <div class="container position-relative">
            <div class="row d-flex justify-content-center">
              <div class="col-lg-6 text-center">
                @if (session('status'))
                <div id="alert" class="alert alert-success">
                  {{ session('status') }}
                </div>
                @endif
                <span class="justify-content-center">
                    <h2 class="font-size-lg">  <i class="bi bi-camera"></i> MotionPlus Studios</h2>
                  </span>

                <h3 class="mt-5">Login</h3>

                <p class="mt-2">Login to enjoy admin privileges</p>

              </div>
            </div>
          </div>
        </div><!-- End Page Header -->

        <section class="sample-page">
          <div class="container" data-aos="fade-up">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <form class="container-centered" method="POST" action=" {{ route('login.store')}}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group mb-2">
                            <label class="form-label" for="project-title">Email</label>
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="form-group mb-2">
                            <label class="form-label" for="project-title">Password</label>
                            <input type="password" class="form-control"  name="password" required autocomplete="current-password"/>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                        </div>

                        <div class="row mb-3 mt-2">
                            <div class="col-md-4 offset-md-8">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember">

                                    <label class="form-check-label" for="remember">
                                     Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-0 mt-2">
                            <div class="col-md-10 offset-md-2">
                                <button type="submit" class="btn btn-primary btn-sm w-150">
                                    Login
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                       Forgot Your Password?
                                    </a>
                                @endif
                            </div>
                        </div>

                     </form>
                </div>
            </div>

          </div>
        </section>


      </main><!-- End #main -->
</x-login>
