<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\Login;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TestimonialsController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\AppointmentsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AdminServicesController;
use Illuminate\Http\Request;
use App\Http\Middleware\AuthCheck;
use App\Http\Middleware\Authenticate;

Route::middleware(['web','authcheck'])->group(function () {
    Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('/login', [LoginController::class, 'login']);
    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

});


Route::middleware(['web','authcheck'])->name('admin.')->group(function () {
    Route::get('/admin/home',[PostsController::class, 'home'])->name('home');
    Route::get('/admin/posts/',[PostsController::class, 'index'])->name('posts.index');
    Route::get('/admin/posts/create', [PostsController::class, 'create'])->name('posts.create');
    Route::post('/admin/posts', [PostsController::class, 'store'])->name('posts.store');
    Route::get('/admin/posts/{id}/edit', [PostsController::class, 'edit'])->name('posts.edit');
    Route::put('/admin/posts/{id}', [PostsController::class, 'update'])->name('posts.update');
    Route::delete('/admin/posts/{id}', [PostsController::class, 'destroy'])->name('posts.destroy');
});

Route::middleware(['web','authcheck'])->group(function () {
    Route::get('/admin',[DashboardController::class, 'index'])->name('admin.dashboard');
    Route::get('/google/analytics/authorize',[DashboardController::class, 'authorize']);

});


Route::middleware(['web','authcheck'])->name('admin.')->group(function () {
    Route::get('/admin/testimonials', [TestimonialsController::class, 'index'])->name('testimonials.index');
    Route::get('/admin/testimonials/create', [TestimonialsController::class, 'create'])->name('testimonials.create');
    Route::post('/admin/testimonials', [TestimonialsController::class, 'store'])->name('testimonials.store');
    Route::get('/admin/testimonials/{id}/edit', [TestimonialsController::class, 'edit'])->name('testimonials.edit');
    Route::put('/admin/testimonials/{id}', [TestimonialsController::class, 'update'])->name('testimonials.update');
    Route::delete('/admin/testimonials/{id}', [TestimonialsController::class, 'destroy'])->name('testimonials.destroy');
});

Route::middleware(['web','authcheck'])->name('admin.')->group(function () {
    Route::get('/admin/services', [ServicesController::class, 'index'])->name('services.index');
    Route::get('/admin/services/create', [ServicesController::class, 'create'])->name('services.create');
    Route::post('/admin/services', [ServicesController::class, 'store'])->name('services.store');
    Route::get('/admin/services/{id}/edit', [ServicesController::class, 'edit'])->name('services.edit');
    Route::put('/admin/services/{id}', [ServicesController::class, 'update'])->name('services.update');
    Route::delete('/admin/services/{id}', [ServicesController::class, 'destroy'])->name('services.destroy');
});


Route::middleware(['web','authcheck'])->name('admin.')->group(function () {
    Route::get('/admin/categories', [CategoriesController::class, 'index'])->name('categories.index');
    Route::get('/admin/categories/create', [CategoriesController::class, 'create'])->name('categories.create');
    Route::post('/admin/categories', [CategoriesController::class, 'store'])->name('categories.store');
    Route::get('/admin/categories/{id}/edit', [CategoriesController::class, 'edit'])->name('categories.edit');
    Route::put('/admin/categories/{id}', [CategoriesController::class, 'update'])->name('categories.update');
    Route::delete('/admin/categories/{id}', [CategoriesController::class, 'destroy'])->name('categories.destroy');
});

Route::middleware(['web','authcheck'])->name('admin.')->group(function () {
    Route::get('/admin/appointments', [AppointmentsController::class, 'index'])->name('appointments.index');
    Route::put('/admin/appointments/{id}', [AppointmentsController::class, 'update'])->name('appointments.update');
    Route::delete('/admin/appointments/{id}', [AppointmentsController::class, 'destroy'])->name('appointments.destroy');
});
Route::middleware(['web','authcheck'])->name('admin.')->group(function () {
    Route::get('/admin/profile/{id}/edit', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::put('/admin/profile/{id}', [ProfileController::class, 'update'])->name('profile.update');
});

Route::middleware(['web'])->group(function () {
    Route::get('/', [HomeController::class, 'home'])->name('index');
    Route::get('/portfolio', [PostsController::class, 'display'])->name('posts.index');
    Route::get('/show/{id}', [PostsController::class, 'show'])->name('show');
    Route::get('/services', [ServicesController::class, 'display'])->name('services.index');
    Route::get('/about', [AboutController::class, 'index'])->name('about');
    Route::get('/contact', [AppointmentsController::class, 'contact'])->name('contact');
    Route::post('/contact', [AppointmentsController::class, 'store'])->name('contact.store');
    Route::get('/gallery/{categoryId?}', [GalleryController::class, 'index'])->name('gallery.index');
    Route::get('emails/contact/{id}', [AppointmentsController::class, 'display']);
});









Auth::routes();

